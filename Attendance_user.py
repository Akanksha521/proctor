import cv2


cam = cv2.VideoCapture(0)

face_detector = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

count = 0
frame_number = 0
attendance_count = 0
while(True):
    ret, img = cam.read()
    if ret==True :
        frame_number += 1
##    print(img)
    #gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        #print(frame_number)
        if frame_number % 30 == 0 :
            print(frame_number)
            
            faces = face_detector.detectMultiScale(img, 1.3, 5)
    
            if len(faces)>=0:
                attendance_count += 1
            
            for (x,y,w,h) in faces:
                x1 = x
                y1 = y
                x2 = x+w
                y2 = y+h
                cv2.rectangle(img, (x1,y1), (x2,y2), (255,255,255), 2)     
                count += 1

        cv2.imshow('image', img)
    k = cv2.waitKey(1) & 0xff # Press 'ESC' for exiting video
    if k == 27:
        break
##    elif count >= 1: # Take 30 face sample and stop video
##         break
print("Attendance Count",attendance_count)
cam.release()
cv2.destroyAllWindows()
