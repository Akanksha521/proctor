from keras.preprocessing.image import img_to_array
import imutils
import cv2
from keras.models import load_model
import numpy as np

# parameters for loading data and images
detection_model_path = 'haarcascade_files/haarcascade_frontalface_default.xml'
emotion_model_path = 'models/emotion_model.hdf5'

# hyper-parameters for bounding boxes shape
# loading models
face_detection = cv2.CascadeClassifier(detection_model_path)
emotion_classifier = load_model(emotion_model_path, compile=False)
EMOTIONS = ["angry" ,"disgust","scared", "happy", "sad", "surprised",
 "neutral"]


#feelings_faces = []
#for index, emotion in enumerate(EMOTIONS):
   # feelings_faces.append(cv2.imread('emojis/' + emotion + '.png', -1))

# starting video streaming
cv2.namedWindow('your_face')


def emotion(frame,faces,gray) :
    

    canvas = np.zeros((250, 300, 3), dtype="uint8")
    canvas2 = np.zeros((250, 300, 3), dtype="uint8")
    frameClone = frame.copy()
    
    if len(faces) > 0:
        faces = sorted(faces, reverse=True,
        key=lambda x: (x[2] - x[0]) * (x[3] - x[1]))[0]
        (fX, fY, fW, fH) = faces
                    # Extract the ROI of the face from the grayscale image, resize it to a fixed 28x28 pixels, and then prepare
            # the ROI for classification via the CNN
        roi = gray[fY:fY + fH, fX:fX + fW]
        roi = cv2.resize(roi, (64, 64))
        roi = roi.astype("float") / 255.0
        roi = img_to_array(roi)
        roi = np.expand_dims(roi, axis=0)
        
        
        preds = emotion_classifier.predict(roi)[0]
        emotion_probability = np.max(preds)
        label = EMOTIONS[preds.argmax()]
    
    

    neg = 0
    pos = 0
    neut =0
    for j in range(len(EMOTIONS)):
        if EMOTIONS[j] == "angry" or EMOTIONS[j] == "disgust" or EMOTIONS[j] == "scared" or EMOTIONS[j] == "sad" :
            neg += preds[j]
        elif EMOTIONS[j] == "neutral " or  j==6:
            neut += preds[j]
        else :
            pos += preds[j]

    emo =["negative","neutral","postive"]
    pred =[]
    pred.append(neg)
    pred.append(neut)
    pred.append(pos)

    for (i,(emotion,prob)) in enumerate(zip(emo , pred)):
        print(emotion,prob)
        text = "{}: {:.2f}%".format(emotion, prob * 100)

        w = int(prob * 300)
        cv2.rectangle(canvas2, (7, (i * 35) + 5),(w, (i * 35) + 35), (0, 0, 255), -1)
        cv2.putText(canvas2, text, (10, (i * 35) + 23),cv2.FONT_HERSHEY_SIMPLEX, 0.45,(255, 255, 255), 2)
                
        

   

                
    for (i, (emotion, prob)) in enumerate(zip(EMOTIONS, preds)):
                # construct the label text
                text = "{}: {:.2f}%".format(emotion, prob * 100)
                
                # draw the label + probability bar on the canvas
               # emoji_face = feelings_faces[np.argmax(preds)]

                
                w = int(prob * 300)
                cv2.rectangle(canvas, (7, (i * 35) + 5),
                (w, (i * 35) + 35), (0, 0, 255), -1)
                cv2.putText(canvas, text, (10, (i * 35) + 23),
                cv2.FONT_HERSHEY_SIMPLEX, 0.45,
                (255, 255, 255), 2)
                cv2.putText(frameClone, label, (fX, fY - 10),
                cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
                cv2.rectangle(frameClone, (fX, fY), (fX + fW, fY + fH),
                              (255, 255, 255), 2)
       
#    for c in range(0, 3):
#        frame[200:320, 10:130, c] = emoji_face[:, :, c] * \
#        (emoji_face[:, :, 3] / 255.0) + frame[200:320,
#        10:130, c] * (1.0 - emoji_face[:, :, 3] / 255.0)


##    cv2.imshow('your_face', frameClone)
##    cv2.imshow("Probabilities", canvas)
##    cv2.imshow("Probabilities for 3 classes", canvas2)

    return frameClone,canvas,canvas2


